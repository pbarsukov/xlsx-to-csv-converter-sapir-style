﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using xlsx2csv4sapir.Xls2CsvGen;

namespace xlsx2csv4sapir
{
    public partial class Form1 : Form
    {
        private string workingFolderName;
        private List<string> foundFiles = new List<string>();
        private List<string> fileNamesOnly = new List<string>();

        private CsvGenerator generator = new CsvGenerator();


        public Form1()
        {
            InitializeComponent();

            barProgressAllFiles.Visible = false;
            btnCsvAllFiles.Enabled = false;
            btnCsvOneFile.Enabled = false;
            lstFileList.DataSource = foundFiles;
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            if (fbdSelectFolder.ShowDialog() == DialogResult.OK)
            {
                workingFolderName = fbdSelectFolder.SelectedPath;
                lblFolderName.Text = workingFolderName;
                foundFiles = getXlsxFileNames();
                fileNamesOnly = foundFiles.Select(file => file.Split('\\').Last()).ToList();
                lstFileList.DataSource = fileNamesOnly;
                btnCsvAllFiles.Enabled = (foundFiles.Count > 0) ? true : false;
                btnCsvOneFile.Enabled = (lstFileList.SelectedIndex != -1) ? true : false;
            }
        }

        private List<string> getXlsxFileNames()
        {
            List<string> allFile = Directory.GetFiles(workingFolderName).ToList();

            return allFile.Where(name => name.Split('.').Last().ToLower().Contains("xlsx")).ToList();
        }

        private void lstFileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnCsvOneFile.Enabled = (lstFileList.SelectedIndex != -1) ? true : false;
        }

        private async void btnCsvOneFile_Click(object sender, EventArgs e)
        {
            string fileName = foundFiles[lstFileList.SelectedIndex];
            setControlsEnabledState(false);
            try
            {
                await Task.Run(() => generator.generateCsv(fileName));
            }
            catch
            {
                MessageBox.Show($"Failed file: {fileName.Split('\\').Last()}", "Error");
            }
            finally
            {
                setControlsEnabledState(true);
            }
            
            //string message = $"Line Number: {generator.lineNum}, Day Type: {generator.dayTypeName}, Total Blocks Count: {generator.numOfBlocks}{Environment.NewLine}{generator.outputText}";
            //MessageBox.Show(message, "Found Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private async void btnCsvAllFiles_Click(object sender, EventArgs e)
        {
            string currentFileName;
            List<string> erroredFiles = new List<string>();

            if (foundFiles.Count == 0)
            {
                return;
            }

            setControlsEnabledState(false);
            barProgressAllFiles.Value = 0;
            barProgressAllFiles.Maximum = foundFiles.Count;
            barProgressAllFiles.Visible = true;

            for (int i = 0; i < foundFiles.Count; i++)
            {
                currentFileName = foundFiles[i];
                try
                {
                    await Task.Run(() => generator.generateCsv(currentFileName));
                }
                catch
                {
                    erroredFiles.Add(fileNamesOnly[i]);
                }
                finally
                {
                    barProgressAllFiles.Value++;
                }
            }

            string resultTitle = "Done";
            string resultMessage = "";
            
            if (erroredFiles.Count > 0)
            {
                resultTitle += " with errors";
                string plural = (foundFiles.Count - erroredFiles.Count > 1) ? "s" : "";
                resultMessage = $"Generated {foundFiles.Count - erroredFiles.Count} CSV file{plural}.{Environment.NewLine}{Environment.NewLine}Failed:{Environment.NewLine}";
                for (int i = 0; i < erroredFiles.Count; i++)
                {
                    resultMessage += $"   {erroredFiles[i]}{Environment.NewLine}";
                }
            }
            else
            {
                string plural = (foundFiles.Count > 1) ? "s" : "";
                resultMessage = $"Generated {foundFiles} CSV file{plural}.";
            }

            MessageBox.Show(resultMessage, resultTitle);
            setControlsEnabledState(true);
            barProgressAllFiles.Visible = false;
        }

        private void setControlsEnabledState(bool state)
        {
            btnCsvOneFile.Enabled = state;
            btnCsvAllFiles.Enabled = state;
            lstFileList.Enabled = state;

            Application.UseWaitCursor = !state;
        }
    }
}
