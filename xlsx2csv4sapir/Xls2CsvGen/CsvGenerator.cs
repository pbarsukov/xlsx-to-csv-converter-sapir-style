﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace xlsx2csv4sapir.Xls2CsvGen
{
    class CsvGenerator
    {
        private Excel.Application excel;
        private Excel.Workbook wb;
        private Excel.Worksheet ws;

        private readonly int lineRow = 2;
        private readonly int lineCol = 7;
        private readonly int dayTypeRow = 7;
        private readonly int dayTypeCol = 5;
        private readonly int blocksRow = 9;
        private readonly int blocksCol = 1;
        private readonly int headerRow = 9;
        private readonly int contentRow = 12;
        private readonly int dataStartCol = 2;
        private readonly int dataEndCol = 52;

        private readonly string targetSheet = "rasp";

        private readonly string separator = ";";

        public string lineNum;
        public string dayTypeName;
        public int numOfBlocks;

        public string outputText;

        public CsvGenerator()
        {
            excel = new Excel.Application();
        }

        public void generateCsv(string inputFileName)
        {
            try
            {
                wb = excel.Workbooks.Open(inputFileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                ws = wb.Sheets[targetSheet];

                lineNum = ws.Cells[lineRow, lineCol].Value2.ToString();
                dayTypeName = ws.Cells[dayTypeRow, dayTypeCol].Value2.ToString();
                numOfBlocks = (int)ws.Cells[blocksRow, blocksCol].Value2;

                outputText = "";
                ReadHeaderData();
                for (int i = 0; i < numOfBlocks; i++)
                {
                    ReadBlockData(i);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (ws != null) 
                { 
                    Marshal.ReleaseComObject(ws); 
                }

                if (wb != null)
                {
                    wb.Close();
                    Marshal.ReleaseComObject(wb);
                }
                
            }

            WriteCsvFile(inputFileName);
        }

        private void ReadHeaderData()
        {
            StringBuilder sb = new StringBuilder();
            string content;
            for (int i = dataStartCol; i <= dataEndCol; i++)
            {
                content = (ws.Cells[headerRow, i].Value2 != null) ? ws.Cells[headerRow, i].Value2.ToString() : "";
                sb.Append($"{content}{separator}");
            }
            sb.Remove(sb.Length - 1, 1); // Remove unneeded trailing separator
            sb.Append(Environment.NewLine);
            outputText = sb.ToString();
        }

        private void ReadBlockData(int numOfBlock)
        {
            List<string[]> blockData = new List<string[]>();
            StringBuilder sbOne = new StringBuilder();
            StringBuilder sbTwo = new StringBuilder();

            string dataOne;
            string dataTwo;

            int rowShift = numOfBlock * 2;

            for (int i = dataStartCol; i <= dataEndCol; i++) // Get DataItems from File into blockData
            {
                dataOne = (ws.Cells[contentRow + rowShift, i].Value2 != null) ? ws.Cells[contentRow + rowShift, i].Value2.ToString() : "";
                dataTwo = (ws.Cells[contentRow + rowShift + 1, i].Value2 != null) ? ws.Cells[contentRow + rowShift + 1, i].Value2.ToString() : "";
                blockData.Add(new string[] { dataOne, dataTwo });
            }

            for (int i = 3; i <= blockData.Count; i++) // Restore Pull-Out time information
            {
                if (blockData[i][1] != "")
                {
                    blockData[1][0] = blockData[i][1];
                    break;
                }
            }

            for (int i = 0; i < blockData.Count; i++) // Create data strings with separators
            {
                sbOne.Append($"{blockData[i][0]}{separator}");
                sbTwo.Append($"{blockData[i][1]}{separator}");
            }

            sbOne.Remove(sbOne.Length - 1, 1); // Remove unneeded trailing separator
            sbTwo.Remove(sbTwo.Length - 1, 1); // Remove unneeded trailing separator

            sbOne.Append(Environment.NewLine);
            sbTwo.Append(Environment.NewLine);

            outputText += sbOne.ToString();
            outputText += sbTwo.ToString();
        }

        private void WriteCsvFile(string sourceFileName)
        {
            string path = sourceFileName.Substring(0, sourceFileName.Length -(sourceFileName.Split('\\').Last().Length));
            string csvFileName = $"{path}{lineNum}-{dayTypeName}.csv";
            File.WriteAllText(csvFileName, outputText, Encoding.GetEncoding(1251));
        }
    }
}
