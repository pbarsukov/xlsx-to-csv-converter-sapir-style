﻿
namespace xlsx2csv4sapir
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fbdSelectFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.lstFileList = new System.Windows.Forms.ListBox();
            this.barProgressAllFiles = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFolderName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.btnCsvOneFile = new System.Windows.Forms.Button();
            this.btnCsvAllFiles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstFileList
            // 
            this.lstFileList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstFileList.FormattingEnabled = true;
            this.lstFileList.HorizontalScrollbar = true;
            this.lstFileList.Location = new System.Drawing.Point(23, 69);
            this.lstFileList.Name = "lstFileList";
            this.lstFileList.Size = new System.Drawing.Size(304, 134);
            this.lstFileList.TabIndex = 0;
            this.lstFileList.SelectedIndexChanged += new System.EventHandler(this.lstFileList_SelectedIndexChanged);
            // 
            // barProgressAllFiles
            // 
            this.barProgressAllFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.barProgressAllFiles.Location = new System.Drawing.Point(364, 180);
            this.barProgressAllFiles.Name = "barProgressAllFiles";
            this.barProgressAllFiles.Size = new System.Drawing.Size(189, 23);
            this.barProgressAllFiles.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Folder:";
            // 
            // lblFolderName
            // 
            this.lblFolderName.AutoEllipsis = true;
            this.lblFolderName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFolderName.Location = new System.Drawing.Point(64, 16);
            this.lblFolderName.Name = "lblFolderName";
            this.lblFolderName.Size = new System.Drawing.Size(232, 25);
            this.lblFolderName.TabIndex = 3;
            this.lblFolderName.Text = " -== Select Folder =-";
            this.lblFolderName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Files:";
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(302, 17);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(25, 23);
            this.btnSelectFolder.TabIndex = 5;
            this.btnSelectFolder.Text = "...";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // btnCsvOneFile
            // 
            this.btnCsvOneFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCsvOneFile.Location = new System.Drawing.Point(396, 79);
            this.btnCsvOneFile.Name = "btnCsvOneFile";
            this.btnCsvOneFile.Size = new System.Drawing.Size(124, 34);
            this.btnCsvOneFile.TabIndex = 6;
            this.btnCsvOneFile.Text = "&CSV from 1 file";
            this.btnCsvOneFile.UseVisualStyleBackColor = true;
            this.btnCsvOneFile.Click += new System.EventHandler(this.btnCsvOneFile_Click);
            // 
            // btnCsvAllFiles
            // 
            this.btnCsvAllFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCsvAllFiles.Location = new System.Drawing.Point(396, 128);
            this.btnCsvAllFiles.Name = "btnCsvAllFiles";
            this.btnCsvAllFiles.Size = new System.Drawing.Size(124, 34);
            this.btnCsvAllFiles.TabIndex = 7;
            this.btnCsvAllFiles.Text = "CSV from &All Files";
            this.btnCsvAllFiles.UseVisualStyleBackColor = true;
            this.btnCsvAllFiles.Click += new System.EventHandler(this.btnCsvAllFiles_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 225);
            this.Controls.Add(this.btnCsvAllFiles);
            this.Controls.Add(this.btnCsvOneFile);
            this.Controls.Add(this.btnSelectFolder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblFolderName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.barProgressAllFiles);
            this.Controls.Add(this.lstFileList);
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "XLSX -> CSV Sapir Format";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog fbdSelectFolder;
        private System.Windows.Forms.ListBox lstFileList;
        private System.Windows.Forms.ProgressBar barProgressAllFiles;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFolderName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.Button btnCsvOneFile;
        private System.Windows.Forms.Button btnCsvAllFiles;
    }
}

