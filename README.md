# CSV Generator for Sapir

Creates CSV file(s) from properly formatted XLSX file(s).

## Requirements and Results

- Source XLSX file should contain sheet named `rasp`.
- Source XLSX file should provide correctly formatted sheet (see sample file). Everything above row 12 should remain unchanged, excluding following cells:
  - `G2` - line number (**shouldn't be empty**);
  - `E7` - day type name (**shouldn't be empty**);
  - `K7` - source stop name (not required for CSV);
  - `N7` - destination stop name (not required for CSV);
  - `A9` - number of blocks (**shouldn't be empty**).
- Starting from row 12 down below all blocks data should be provided.
- Filename for the result is generated based on data in source file: `LineNumber-DayTypeName.CSV`
- Resulting CSV file encoded with `Windows-1251` codepage.
- If any error found during CSV generation the file being process skipped.

## Work process

1. Choose folder.
2. Select XLSX file to generate CSV from or press `"CSV from ALl Files"` button to process all files found.
3. Press `"CSV from 1 file"` button to process selected file only.

## Possible improvements

- Files multiple choice support.
- Request for specific file name for the result.
- Choice the result file codepage.
